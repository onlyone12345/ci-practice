<?php
      if (!function_exists('clientLoginChecker'))
      {
            function clientLoginChecker() {
                  $self = get_instance();
                  $isLoggedIn = $self->session->userdata('client_logged_in');
                  return $isLoggedIn;
            }
      }

      if (!function_exists('adminLoginChecker'))
      {
            function adminLoginChecker() {
                  $self = get_instance();
                  $isLoggedIn = $self->session->userdata('admin_logged_in');
                  return $isLoggedIn;
            }
      }
?>