<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
        parent:: __construct();
        $this->load->model('User_model');
        $this->load->model('Client_model');
    }
    
    public function index()
    {
        redirect('login/client');
    }

    // After this line is admin area

	public function admin()
	{
        $isLoggedIn = adminLoginChecker();
        if ($isLoggedIn)
            redirect('admin');
            
        $this->load->view('login/admin');
    }
    
    public function doAdminLogin()
    {
        $isLoggedIn = adminLoginChecker();
        if ($isLoggedIn)
            redirect('admin');

        $val = array();
        $val["username"] = $this->input->post("username");
        $val["password"] = sha1($this->input->post("password"));
    }

    // After this line is client area

	public function client()
	{
        $isLoggedIn = clientLoginChecker();
        if ($isLoggedIn)
            redirect('client');

        $this->load->view('login/client');
    }
    
    public function doClientLogin()
    {
        $isLoggedIn = clientLoginChecker();
        if ($isLoggedIn)
            redirect('client');

        $where = array();
        $where["username"] = $this->input->post("username");
        $where["password"] = sha1($this->input->post("password"));
        $this->User_model->setWhere($where);
        $user = $this->User_model->getData();
        if (!$user)
            echo "No data";
        else {
            $user = $user[0];
            $userData = array();
            $userData["client_user"] = $user;
            
            $where = array();
            $where["id_user"] = $user->id_user;
            $this->Client_model->setWhere($where);
            $client = $this->Client_model->getData()[0];

            $userData["client_data"] = $client;
            $userData["client_logged_in"] = true;
            $this->session->set_userdata($userData);
            redirect('client');
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */