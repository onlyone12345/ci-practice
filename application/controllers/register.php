<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
        parent:: __construct();
        $this->load->model('User_model');
        $this->load->model('Client_model');
    }
    
    public function index()
    {
        redirect('register/client');
    }

    // After this line is admin area

	public function admin()
	{
        $this->load->view('register/admin');
    }
    
    public function doAdminRegister()
    {
        $val = array();
        $val["username"] = $this->input->post("username");
        $val["password"] = sha1($this->input->post("password"));
    }

    // After this line is client area

	public function client()
	{
        $this->load->view('register/client');
    }
    
    public function doClientRegister()
    {
        $val = array();
        $val["username"] = $this->input->post("username");
        $val["password"] = sha1($this->input->post("password"));
        $val["role"] = 1;
        $val["created_at"] = date("Y-m-d h:i:s");
        $val["updated_at"] = date("Y-m-d h:i:s");
        $this->User_model->setValues($val);
        $userId = $this->User_model->insertData();
        $val = array(); // array harus di re-declare agar isinya kosong
        $val["id_user"] = $userId;
        $val["nama"] = $this->input->post("fullname");
        $val["alamat"] = $this->input->post("adress");
        $val["no_hp"] = $this->input->post("phone");
        $this->Client_model->setValues($val);
        $clientId = $this->Client_model->insertData();
        $this->session->set_flashdata('registration_success', 'Congratulation, you are now registered!');
        redirect('login/client');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */